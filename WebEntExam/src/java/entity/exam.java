/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Date;

/**
 *
 * @author Tai MTP
 */
public class exam {
    protected String subject;
    protected String starttime;
    protected String date;
    protected String duration;
    protected String classroom;
    protected String faculty;
    protected String status;

    public exam() {
    }

    public exam(String subject, String starttime, String date, String duration, String classroom, String faculty, String status) {
        this.subject = subject;
        this.starttime = starttime;
        this.date = date;
        this.duration = duration;
        this.classroom = classroom;
        this.faculty = faculty;
        this.status = status;
    }

    public exam(String subject, String starttime, String date, String duration, String room, String faculty) {
        
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getExamdate() {
        return date;
    }

    public void setExamdate(Date examdate) {
        this.date = date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}
