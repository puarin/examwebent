/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package da;

import entity.exam;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Tai MTP
 */
public class DataAccess {
    DBConnection dbconnect = new DBConnection();
    public boolean addexam(exam exam) {
        try {
            String query = "  INSERT INTO dbo.exam( subject ,starttime ,examdate ,duration ,classroom ,faculty,status)"
                    + "VALUES(?,?,?,?,?,'USER')";
            Connection connection = dbconnect.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, exam.getSubject());
            ps.setString(2, exam.getStarttime());
            ps.setString(3, exam.getExamdate());
            ps.setString(4, exam.getDuration());
            ps.setString(5, exam.getClassroom());
            ps.setString(6, exam.getFaculty());
            ps.setString(7, exam.getStatus());
            
            int result = ps.executeUpdate();
            return result > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
